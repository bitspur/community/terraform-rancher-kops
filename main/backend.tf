/**
 * File: /main/backend.tf
 * Project: kops
 * File Created: 14-04-2022 08:17:29
 * Author: Clay Risser
 * -----
 * Last Modified: 17-09-2022 06:55:25
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 */

terraform {
  backend "http" {}
}
